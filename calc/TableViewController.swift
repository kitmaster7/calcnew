//
//  ViewController.swift
//  calc
//
//  Created by Admin on 27.08.2018.
//  Copyright © 2018 Alex. All rights reserved.
//

import UIKit
import CoreData
class TableViewController: UITableViewController {

 
    var ProcentArray = [Int]()
  
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
   
        print(FileManager.default.urls(for: .documentDirectory, in: .userDomainMask))
        clearTable()
      calc(table: tableView)
   
    }
 
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return sberEntityArray.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Item",for:indexPath)
      
        let item = sberEntityArray[indexPath.row]
    
     //  cell.textLabel?.text = "Дата оплаты: "
    
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let dateString = dateFormatter.string(from:item.payment_date as! Date)
        
        
    cell.textLabel?.text = "Дата: " + dateString +
            " | Основной долг: " + String(item.osnovnoy_dolg) +
            " | Проценты: " + String(item.procent) +
           " | Сум оплате: " + String(  item.summ_for_pay) +
              " | Ост осн длга: " + String( item.ostatok_po_dolgy)
        return cell
    }
    
}
